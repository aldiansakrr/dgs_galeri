<?php

namespace App\Models;

use CodeIgniter\Model;

class Karya extends Model
{
    protected $table            = 'karya';
    protected $primaryKey       = 'id_karya';
    protected $useAutoIncrement = false;
    protected $allowedFields    = [
        'id_karya',
        'nama_karya',
        'id_kategori',
        'pembuat_karya',
        'tahun_pembuatan_karya',
        'harga_karya',
        'keterangan_karya',
    ];

}
