<?= $this->extend('layout/app') ?>

<?= $this->section('content') ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-3">Karya</h1>
	<!-- DataTales Example -->
	<div class="row">
		<div class="col-12">
			<?= view('Karya/table') ?>
			<?= view('Karya/form') ?>
		</div>
	</div>
	<?= view('Karya/javascript') ?>

</div>
<!-- /.container-fluid -->

<?= $this->endSection() ?>