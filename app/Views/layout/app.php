<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?= $page ?> | Gallery DSG</title>

	<!-- Custom fonts for this template-->
	<link href="<?= base_url() ?>sbadmin2/assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>custom/font.css" rel="stylesheet">

	<!-- Line Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url() ?>custom/line-awesome/css/line-awesome.css">
	<!-- Custom styles for this template-->
	<link href="<?= base_url() ?>sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">

	<!-- Custom styles for this page -->
	<link href="<?= base_url() ?>sbadmin2/assets/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link href="<?= base_url() ?>swal/sweetalert2.min.css" rel="stylesheet">
	<script src="<?= base_url() ?>swal/sweetalert2.all.min.js"></script>

	<!-- Select2 -->
	<link href="<?= base_url() ?>custom/select2.min.css" rel="stylesheet" />

	<!-- FavIcon -->
	<link rel="icon" href="<?= base_url() ?>karya.png">

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">


		<?= $this->include('layout/sidebar') ?>

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<?= $this->include('layout/header') ?>

				<?= $this->renderSection('content') ?>

			</div>
			<!-- End of Main Content -->

			<?= $this->include('layout/footer') ?>

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>


	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_url() ?>sbadmin2/assets/jquery/jquery.min.js"></script>
	<script src="<?= base_url() ?>sbadmin2/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_url() ?>sbadmin2/assets/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_url() ?>sbadmin2/js/sb-admin-2.min.js"></script>

	<!-- Page level plugins -->
	<script src="<?= base_url() ?>sbadmin2/assets/chart.js/Chart.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="<?= base_url() ?>sbadmin2/js/demo/chart-area-demo.js"></script>
	<script src="<?= base_url() ?>sbadmin2/js/demo/chart-pie-demo.js"></script>

	<!-- Page level plugins -->
	<script src="<?= base_url() ?>sbadmin2/assets/datatables/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>sbadmin2/assets/datatables/dataTables.bootstrap4.min.js"></script>

	<!-- Page level custom scripts -->
	<script src="<?= base_url() ?>sbadmin2/js/demo/datatables-demo.js"></script>

	<!-- BlockUI -->
	<script src="<?= base_url() ?>custom/blockui.js"></script>

	<!-- select2 -->
	<script src="<?= base_url() ?>custom/select2.min.js"></script>

	<!-- Number -->
	<script src="<?= base_url() ?>custom/jquery.number.js"></script>

	<!-- MomentJS -->
	<script src="<?= base_url() ?>custom/moment.js"></script>

	<!-- Custom Helper -->
	<script src="<?= base_url() ?>custom/helper.js"></script>

	<script>
		var BASE_URL = "<?= base_url() ?>";
	</script>

	<?= $this->renderSection('script') ?>

</body>

</html>