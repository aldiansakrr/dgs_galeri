<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');


// Route untuk data Kategori
$routes->group('kategori', function ($routes) {
	$routes->get('', 'KategoriController::index');
	$routes->post('fetch', 'KategoriController::fetch');
	$routes->post('store', 'KategoriController::store');
	$routes->post('show', 'KategoriController::show');
	$routes->post('delete', 'KategoriController::delete');
	$routes->post('combo', 'KategoriController::combo');
});

// Route untuk data Karya
$routes->group('karya', function ($routes) {
	$routes->get('', 'KaryaController::index');
	$routes->post('fetch', 'KaryaController::fetch');
	$routes->post('store', 'KaryaController::store');
	$routes->post('show', 'KaryaController::show');
	$routes->post('delete', 'KaryaController::delete');
});
