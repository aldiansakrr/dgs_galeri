var HELPER = function () {
	var loadBlock = function (message = 'Loading...') {
		$.blockUI({
			message:
				`<div class="blockui-message" style="z-index: 9999">
				<span class="spinner-border text-primary"></span> ${message} </div>
				`,
			css: {
				border: 'none',
				backgroundColor: 'rgba(47, 53, 59, 0)',
				'z-index': 9999
			}
		});
	}

	var unblock = function (delay) {
		window.setTimeout(function () {
			$.unblockUI();
		}, delay);
	}

	return {
		block: function (msg) {
			loadBlock(msg);
		},

		unblock: function (delay) {
			unblock(delay);
		},

		table: function (config) {
			HELPER.block()

			$(config.table).DataTable({
				processing: true,
				serverSide: true,
				ajax: {
					url: config.url,
					method: 'POST'
				},
				order: [],
				columnDefs: [{
					targets: 0,
					orderable: false
				},
				{
					targets: -1,
					orderable: false
				}

				]
			});

			HELPER.unblock()
		},

		showMessage: function (config) {
			Swal.fire({
				icon: config.success == true ? "success" : "error",
				text: config.text,
				title: config.success == true ? "Success" : "Error",
			})
		},

		confirmDelete: function (config) {
			Swal.fire({
				title: "Yakin hapus?",
				text: "Data yang akan dihapus tidak dapat dikembalikan!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Ya, hapus!",
				cancelButtonText: "Batalkan",
			}).then((result) => {
				if (result.isConfirmed) {
					HELPER.block();
					$.ajax({
						data: config.data,
						url: config.url,
						method: "POST",
						success: function (res) {
							if (res.success) {
								HELPER.showMessage({
									success: true,
									text: res.message,
								})
							}
						}
					})
					HELPER.resetForm(config.form)
					$('#formTitle').html($('#formTitle').data('old-state'));
					$(config.table).DataTable().ajax.reload();
					HELPER.unblock();
				}
			});
		},

		refresh: function (config) {
			HELPER.block();
			HELPER.resetForm(config.form)
			$('#formTitle').html($('#formTitle').data('old-state'));
			$(config.table).DataTable().ajax.reload();
			HELPER.unblock();
		},

		show: function (config) {
			HELPER.block();
			$.ajax({
				data: config.data,
				url: config.url,
				method: "POST",
				success: function (res) {
					$('#formTitle').html(config.title);
					HELPER.populateForm(config.form, res.data);
				}
			})
			HELPER.unblock();
		},

		populateForm: function (frm, data) {
			$.each(data, function (key, value) {
				var $ctrl = $('[name="' + key + '"]', frm);
				if ($ctrl.is('select')) {
					if ($ctrl.data().hasOwnProperty('select2')) {
						$ctrl.val(value);
						$ctrl.trigger('change');
					} else {
						$("option", $ctrl).each(function () {
							if (this.value == value) {
								this.selected = true;
							}
						});
					}
				} else if ($ctrl.is('textarea')) {
					if (typeof $ctrl.data('_inputmask_opts') != 'undefined') {
						var inputmask_opt = $ctrl.data('_inputmask_opts')
						if (inputmask_opt.hasOwnProperty('digits')) {
							$ctrl.val(parseFloat(value));
						} else {
							$ctrl.val(value);
						}
					} else if (typeof $ctrl.data('mousewheelLineHeight') != 'undefined') {
						if (value != "" && value.length > 5) {
							$ctrl.clockTimePicker('value', value.substring(0, 5));
						} else {
							$ctrl.val(value);
						}
					} else {
						$ctrl.val(value);
					}
				} else {
					switch ($ctrl.attr("type")) {
						case "text":
						case "date":
						case "email":
						case "number":
						case "hidden":
						case "color":
						case "time":
							if (typeof $ctrl.data('_inputmask_opts') != 'undefined') {
								var inputmask_opt = $ctrl.data('_inputmask_opts')
								if (inputmask_opt.hasOwnProperty('digits')) {
									$ctrl.val(parseFloat(value));
								} else {
									$ctrl.val(value);
								}
							} else if (typeof $ctrl.data('mousewheelLineHeight') != 'undefined') {
								if (value != "" && value.length > 5) {
									$ctrl.clockTimePicker('value', value.substring(0, 5));
								} else {
									$ctrl.val(value);
								}
							} else {
								$ctrl.val(value);
							}
							break;
						case "radio":
						case "checkbox":
							$ctrl.each(function () {
								if ($(this).attr('value') == value) {
									$(this).prop('checked', true);
								} else {
									$(this).prop('checked', false);
								}
							});
							break;
					}
				}
			});
		},

		ajax: function (config) {

			HELPER.block();

			$.ajax({
				type: 'POST',
				url: config.url,
				data: config.data,
				success: function (response) {
					if (response.success) {
						HELPER.showMessage({
							success: true,
							text: response.message,
						})
					} else {
						HELPER.showMessage({
							success: false,
							text: response.message,
						})
					}
					HELPER.resetForm(config.form)
					$('#formTitle').html($('#formTitle').data('old-state'));
					$(config.table).DataTable().ajax.reload();
				},
				error: function (error) {
					HELPER.showMessage({
						success: false,
						text: error.statusText,
					})
				}
			})

			HELPER.unblock();

		},

		select2: function (config) {
			HELPER.block();

			$(config.el).select2({
				placeholder: (config.placeholder) ? config.placeholder : '',
				allowClear: true,
				width: '100%',
			});

			HELPER.unblock();
		},

		resetForm: function (form = '') {
			HELPER.block()

			$('input[type="radio"]').prop('checked', false);
			$('input[type="checkbox"]').prop('checked', false);
			$(form).find(`textarea,input:not('[type="checkbox"], [type="radio"]'),select`).val("").change()

			HELPER.unblock()
		},

		hide: function (config) {
			HELPER.block()

			$("#" + config.hide).hide(750);
			$("#" + config.show).show(750);

			HELPER.unblock()
		},
		
		create_combo: function (config) {
			config = $.extend(true, {
				el: null,
				valueField: null,
				displayField: null,
				displayField2: null,
				displayField3: null,
				parentField: 'parent',
				childField: 'child',
				url: null,
				grouped: false,
				nesting: false,
				withNull: true,
				data: null,
				chosen: false,
				callback: function () { }
			}, config);

			if (config.url !== null) {
				$.ajax({
					url: config.url,
					data: config.data,
					type: 'POST',
					complete: function (response) {
						var html = (config.withNull === true) ? "<option value>-Pilih-</option>" : "";
						var data = $.parseJSON(response.responseText);
						if (data.success) {
							$.each(data.data, function (i, v) {
								if (config.nesting) {
									// only 2 level tree
									if (v['children']) {
										var temp_parent = v[config.valueField];
										html += "<optgroup value='" + v[config.valueField] + "' label='" + v[config.displayField] + "'>";
										$.each(v[config.childField], function (n_i, n_v) {
											if (config.grouped) {
												if (config.displayField3 != null) {
													html += "<option value='" + n_v[config.valueField] + "'>" + n_v[config.displayField] + " - " + n_v[config.displayField2] + " ( " + n_v[config.displayField3] + " ) " + "</option>";
												}
												else {
													html += "<option value='" + n_v[config.valueField] + "'>" + n_v[config.displayField] + " - " + n_v[config.displayField2] + "</option>";
												}
											} else {
												html += "<option value='" + n_v[config.valueField] + "'>" + n_v[config.displayField] + "</option>";
											}
										})
										html += "</optgroup>";
									} else {
										html += "<optgroup value='" + v[config.valueField] + "' label='" + v[config.displayField] + "'>";
										if (config.grouped) {
											if (config.displayField3 != null) {
												html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField] + " - " + v[config.displayField2] + " ( " + v[config.displayField3] + " ) " + "</option>";
											}
											else {
												html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField] + " - " + v[config.displayField2] + "</option>";
											}
										} else {
											html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField] + "</option>";
										}
										html += "</optgroup>";
									}
								} else {
									if (config.grouped) {
										if (config.displayField3 != null) {
											html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField2] + " - " + v[config.displayField] + " ( " + v[config.displayField3] + " ) " + "</option>";
										}
										else {
											html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField] + " - " + v[config.displayField2] + "</option>";
										}
									} else {
										html += "<option value='" + v[config.valueField] + "'>" + v[config.displayField] + "</option>";
									}
								}
							});
							if (config.el.constructor === Array) {
								$.each(config.el, function (i, v) {
									$('#' + v).html(html);
								})
							}
							else {
								$('#' + config.el).html(html);
							}
							if (config.chosen) {
								if (config.el.constructor === Array) {
									$.each(config.el, function (i, v) {
										$('#' + v).addClass(v);
										$('.' + v).select2({
											allowClear: true,
											dropdownAutoWidth: true,
											placeholder: "-Pilih-",
											width: '100%'
										});
									})
								} else {
									$('#' + config.el).addClass(config.el);
									$('.' + config.el).select2({
										allowClear: true,
										dropdownAutoWidth: true,
										placeholder: "-Pilih-",
										width: '100%'
									});
								}

							} else {
								if (config.el.constructor === Array) {
									$.each(config.el, function (i, v) {
										$('#' + v).addClass(v);
										$('.' + v).select2({
											allowClear: true,
											dropdownAutoWidth: true,
											placeholder: "-Pilih-",
											width: '100%',
											tags: true,
										});
									})
								} else {
									$('#' + config.el).addClass(config.el);
									$('.' + config.el).select2({
										allowClear: true,
										dropdownAutoWidth: true,
										placeholder: "-Pilih-",
										width: '100%',
										tags: true,
									});
								}
							}
						}
						config.callback(data);
					}
				});
			}
			else {
				var response = { success: false, message: 'Url kosong' };
				config.callback(response);
			}
		},

	}
}();