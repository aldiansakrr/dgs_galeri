<?= $this->extend('layout/app') ?>

<?= $this->section('script') ?>

<script>
	var table = "#karyaTable";
	var form = "#formKarya";

	$(document).ready(function() {
		$('#formTitle').data('old-state', $('#formTitle').html());

		HELPER.table({
			url: BASE_URL + '/karya/fetch',
			table: table,
		})

		HELPER.hide({
			hide: 'cardFormKarya',
			show: 'cardTableKarya',
		})

		HELPER.create_combo({
			el: 'id_kategori',
			url: BASE_URL + 'kategori/combo',
			valueField: 'id_kategori',
			displayField: 'nama_kategori',
			withNull: true,
		})

		$("#harga_karya").number(true);
		// $("#tahun_pembuatan_karya").number(true);

	});


	function onAdd() {
		HELPER.resetForm(form);
		$('#formTitle').html("Tambah Karya");
		HELPER.hide({
			hide: 'cardTableKarya',
			show: 'cardFormKarya',
		})
	}

	function onBack() {
		HELPER.hide({
			hide: 'cardFormKarya',
			show: 'cardTableKarya',
		})
	}

	function onSave() {
		if (!$("#nama_karya").val()) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Nama Karya tidak boleh kosong'
			})
			return
		}

		if (!$("#id_kategori").val()) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Kategori Karya tidak boleh kosong'
			})
			return
		}

		if (!$("#pembuat_karya").val()) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Pembuat Karya tidak boleh kosong'
			})
			return
		}

		if (!$("#tahun_pembuatan_karya").val() || $("#tahun_pembuatan_karya").val() < 1000) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Tahun Pembuatan Karya tidak boleh kosong atau tahun tidak valid'
			})
			return
		}

		if (!$("#harga_karya").val() || $("#harga_karya").val() <= 0) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Harga Karya tidak boleh kosong atau harga tidak valid'
			})
			return
		}

		if (!$("#keterangan_karya").val()) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Keterangan karya tidak boleh kosong'
			})
			return
		}

		HELPER.ajax({
			data: $(form).serialize(),
			url: BASE_URL + "/karya/store",
			form: form,
			table: table,
		})


		HELPER.hide({
			hide: 'cardFormKarya',
			show: 'cardTableKarya',
		})

	}

	function onEdit(el) {
		HELPER.show({
			data: {
				id_karya: el,
			},
			url: BASE_URL + "/karya/show",
			form: formKarya,
			title: 'Edit Karya',
		});
		onAdd()
		$('#formTitle').html("Edit Karya");
	}

	function onDelete(el) {
		HELPER.confirmDelete({
			data: {
				id_karya: el,
			},
			url: BASE_URL + '/karya/delete',
			form: form,
			table: table,
		})
	}

	function onRefresh() {
		$('#formTitle').data('old-state', $('#formTitle').html());
		HELPER.refresh({
			form: form,
			table: table
		})
	}

	function onReset() {
		$('#formTitle').data('old-state', $('#formTitle').html());
		HELPER.resetForm(form)
	}
</script>

<?= $this->endSection() ?>