		<!-- Sidebar -->
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Sidebar - Brand -->
			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
				<div class="sidebar-brand-icon rotate-n-15">
					<i class="las la-image"></i>
				</div>
				<div class="sidebar-brand-text mx-3">Gallery DSG</div>
			</a>

			<!-- Divider -->
			<hr class="sidebar-divider my-0">

			<!-- Nav Item - Dashboard -->
			<li class="nav-item <?= $page == "Dashboard" ? "active" : "" ?>">
				<a class="nav-link" href="<?= base_url() ?>">
					<i class="fas fa-fw fa-tachometer-alt"></i>
					<span>Dashboard</span></a>
			</li>


			<!-- Divider -->
			<hr class="sidebar-divider">

			<!-- Heading -->
			<div class="sidebar-heading">
				Master Data
			</div>

			<!-- Nav Item - Kategori -->
			<li class="nav-item <?= $page == "Data Kategori" ? "active" : "" ?>">
				<a class="nav-link" href="<?= base_url() ?>kategori/">
					<i class="fas fa-fw fa-image"></i>
					<span>Kategori</span></a>
			</li>

			<!-- Nav Item - Karya -->
			<li class="nav-item <?= $page == "Data Karya" ? "active" : "" ?>">
				<a class="nav-link" href="<?= base_url() ?>karya/">
					<i class="fas fa-fw fa-image"></i>
					<span>Karya</span></a>
			</li>

		</ul>
		<!-- End of Sidebar -->