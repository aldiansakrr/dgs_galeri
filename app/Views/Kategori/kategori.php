<?= $this->extend('layout/app') ?>

<?= $this->section('content') ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800 mb-4">Kategori</h1>
	<!-- DataTales Example -->
	<div class="row">
		<div class="col-4">
			<?= view('Kategori/form') ?>
		</div>
		<div class="col-8">
			<?= view('Kategori/table') ?>
		</div>
	</div>
	<?= view('Kategori/javascript') ?>

</div>
<!-- /.container-fluid -->

<?= $this->endSection() ?>