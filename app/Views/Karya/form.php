<div class="card shadow mb-4" id="cardFormKarya">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-4">
				<h6 class="m-0 font-weight-bold text-primary" id="formTitle">Tambah Karya</h6>
			</div>
			<div class="col-8 mb-n5">
				<button type="button" onclick="onBack()" class="mb-5 btn btn-success float-right"><i class="las la-arrow-circle-left"></i> Kembali</button>
			</div>
		</div>
	</div>
	<form action="javascript:onSave()" method="post" name="formKarya" id="formKarya">
		<div class="card-body">
			<div class="form-group">
				<label for="" class="col-form-label">Nama Karya</label>
				<input type="text" name="id_karya" id="id_karya" style="display: none;">
				<input type="text" class="form-control" name="nama_karya" id="nama_karya" placeholder="Nama Karya">
			</div>
			<div class="form-group">
				<label for="" class="col-form-label">Kategori Karya</label>
				<select name="id_kategori" id="id_kategori" class="form-control"></select>
			</div>
			<div class="form-group">
				<label for="" class="col-form-label">Pembuat Karya</label>
				<input type="text" name="pembuat_karya" id="pembuat_karya" class="form-control" placeholder="Pembuat Karya">
			</div>
			<div class="form-group">
				<label for="" class="col-form-label">Tahun Pembuatan Karya</label>
				<input type="number" min="0" name="tahun_pembuatan_karya" id="tahun_pembuatan_karya" class="form-control" placeholder="2024">
			</div>
			<div class="form-group">
				<label for="" class="col-form-label">Harga Karya</label>
				<input type="text" name="harga_karya" id="harga_karya" class="form-control" placeholder="25000">
			</div>
			<div class="form-group mb-4">
				<label for="" class="col-form-label">Keterangan Karya</label>
				<textarea name="keterangan_karya" id="keterangan_karya" class="form-control" placeholder="Keterangan Karya"></textarea>
			</div>
			<div class="form-group mb-5">
				<button type="reset" onclick="onReset()" class="btn btn-info float-left">Reset</button>
				<button type="submit" class="btn btn-primary float-right">Submit</button>
			</div>
		</div>
	</form>
</div>