<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Karya;
use Hermawan\DataTables\DataTable;

class KaryaController extends BaseController
{
    public $karya;

    public function __construct()
    {
        $this->karya = new Karya();
    }

    public function index()
    {
        $data = [
            'page' => 'Data Karya'
        ];
        return view('Karya/karya', $data);
    }

    public function fetch()
    {
        $db = db_connect();
        $builder = $db->table('karya')
            ->select('karya.id_karya, 
            karya.nama_karya, 
            kategori.nama_kategori, 
            karya.pembuat_karya, 
            karya.tahun_pembuatan_karya, 
            karya.harga_karya,
            karya.keterangan_karya
            ')
            ->join('kategori', 'karya.id_kategori = kategori.id_kategori');
        return DataTable::of($builder)
            ->setSearchableColumns(['kategori.nama_kategori'])
            ->addNumbering('no')
            ->add('action', function ($row) {
                return '
                <button type="button" class="btn btn-primary btn-sm text-center" onclick="onEdit(' . $row->id_karya . ')"><i class="fas fa-edit"></i> Edit</button>
                <button type="button" class="btn btn-danger btn-sm text-center" onclick="onDelete(' . $row->id_karya . ')"><i class="fas fa-trash"></i> Hapus</button>
                ';
            })
            ->format('harga_karya', function ($value) {
                return 'Rp' . number_format($value);
            })
            ->toJson();
    }

    public function show()
    {
        $id = $this->request->getPost('id_karya');

        $query = $this->karya->find($id);

        return $this->response->setJSON([
            'success' => TRUE,
            'data' => $query
        ]);
    }

    public function store()
    {
        // ambil semua data dari inputan
        $data = $this->request->getPost();

        if ($data['id_karya'] != NULL) {
            $this->karya->save($data);
            return $this->response->setJSON([
                'success' => TRUE,
                'message' => 'Data Karya berhasil diubah'
            ]);
        } else {
            $this->karya->insert($data);
            return $this->response->setJSON([
                'success' => TRUE,
                'message' => 'Data Karya berhasil ditambahkan'
            ]);
        }
    }

    public function delete()
    {
        $data = $this->request->getPost();

        $this->karya->delete($data);
        return $this->response->setJSON([
            'success' => TRUE,
            'message' => 'Data Karya berhasil dihapus'
        ]);
    }
}
