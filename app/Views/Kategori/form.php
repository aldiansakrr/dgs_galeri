<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary" id="formTitle">Tambah Kategori</h6>
	</div>
	<form action="javascript:onSave()" method="post" name="formKategori" id="formKategori">
		<div class="card-body">
			<div class="form-group">
				<label for="" class="col-form-label">Nama Kategori</label>
				<input type="text" name="id_kategori" id="id_kategori" style="display: none;">
				<input type="text" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Nama Kategori">
			</div>
			<div class="form-group mb-5">
				<button type="reset" onclick="onReset()" class="btn btn-info float-left">Reset</button>
				<button type="submit" class="btn btn-primary float-right">Submit</button>
			</div>
		</div>
	</form>
</div>