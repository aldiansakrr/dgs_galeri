<?= $this->extend('layout/app') ?>

<?= $this->section('script') ?>

<script>
	var table = "#kategoriTable";
	var form = "#formKategori";

	$(document).ready(function() {
		$('#formTitle').data('old-state', $('#formTitle').html());

		HELPER.table({
			url: BASE_URL + '/kategori/fetch',
			table: table,
		})
	});


	function onSave() {
		if (!$("#nama_kategori").val()) {
			HELPER.showMessage({
				success: false,
				title: "Error",
				text: 'Nama kategori tidak boleh kosong'
			})
			return;
		}

		HELPER.ajax({
			data: $(form).serialize(),
			url: BASE_URL + "/kategori/store",
			form: form,
			table: table,
		})

	}

	function onEdit(el) {
		HELPER.show({
			data: {
				id_kategori: el,
			},
			url: BASE_URL + "/kategori/show",
			form: formKategori,
			title: 'Edit Kategori',
		});
	}

	function onDelete(el) {
		HELPER.confirmDelete({
			data: {
				id_kategori: el,
			},
			url: BASE_URL + '/kategori/delete',
			form: form,
			table: table,
		})
	}

	function onRefresh() {
		HELPER.refresh({
			form: form,
			table: table
		})
	}

	function onReset() {
		$('#formTitle').data('old-state', $('#formTitle').html());
		HELPER.resetForm(form)
	}
</script>

<?= $this->endSection() ?>