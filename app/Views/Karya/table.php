<div class="card shadow mb-4" id="cardTableKarya">
	<div class="card-header py-3">
		<div class="row">
			<div class="col-4">
				<h6 class="font-weight-bold text-primary"><?= $page ?></h6>
			</div>
			<div class="col-8 mb-n5">
				<button type="button" onclick="onRefresh()" class="mb-5 btn btn-success float-right"><i class="las la-sync"></i> Refresh</button>
				<button type="button" onclick="onAdd()" class="mx-3 btn btn-primary float-right"><i class="fas fa-plus"></i> Tambah</button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered text-center" id="karyaTable" width="100%" cellspacing="0">
				<thead class="text-center">
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Nama</th>
						<th>Kategori</th>
						<th>Pembuat</th>
						<th>Tahun Pembuatan</th>
						<th>Harga</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tfoot class="text-center">
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Nama</th>
						<th>Kategori</th>
						<th>Pembuat</th>
						<th>Tahun Pembuatan</th>
						<th>Harga</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>