<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Kategori;
use Hermawan\DataTables\DataTable;

class KategoriController extends BaseController
{
    public $kategori;

    public function __construct()
    {
        $this->kategori = new Kategori();
    }

    public function index()
    {
        $data = [
            'page' => 'Data Kategori'
        ];
        return view('Kategori/kategori', $data);
    }

    public function fetch()
    {
        return DataTable::of($this->kategori)
            ->addNumbering('no')
            ->add('action', function ($row) {
                return '
                <button type="button" class="btn btn-primary btn-sm text-center" onclick="onEdit(' . $row->id_kategori . ')"><i class="fas fa-edit"></i> Edit</button>
                <button type="button" class="btn btn-danger btn-sm text-center" onclick="onDelete(' . $row->id_kategori . ')"><i class="fas fa-trash"></i> Hapus</button>
                ';
            })
            ->hide('id_kategori')
            ->toJson();
    }

    public function show()
    {
        $id = $this->request->getPost('id_kategori');

        $query = $this->kategori->find($id);

        return $this->response->setJSON([
            'success' => TRUE,
            'data' => $query
        ]);
    }

    public function store()
    {
        // ambil semua data dari inputan
        $data = $this->request->getPost();

        if ($data['id_kategori'] != NULL) {
            $this->kategori->save($data);
            return $this->response->setJSON([
                'success' => TRUE,
                'message' => 'Data kategori berhasil diubah'
            ]);
        } else {
            $this->kategori->insert($data);
            return $this->response->setJSON([
                'success' => TRUE,
                'message' => 'Data kategori berhasil ditambahkan'
            ]);
        }
    }

    public function delete()
    {
        $data = $this->request->getPost();

        $this->kategori->delete($data);
        return $this->response->setJSON([
            'success' => TRUE,
            'message' => 'Data kategori berhasil dihapus'
        ]);
    }

    public function combo()
    {
        $query = $this->kategori->findAll();

        return $this->response->setJSON([
            'success' => true,
            'data' =>  $query,
        ]);
    }
}
