<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data = [
            'page' => 'Dashboard'
        ];

        return view('index', $data);
    }
}
